---
title: plain and simple state machine
---

Recently I have made a breakthrough. I mean I broke through thick layer of reinforced concrete what happen to be the mindset of my coworker. A miracle indeed. After painful yet unresolved deliberation a seed of an idea was planted in his mind. Maybe it resonated with an old, half-forgotten childhood memory. Whatever it was we ended up agreeing to make and use a state machine framework in our upcoming endevour.

You may think "a state machine? what a big deal? stop loitering - get busy already!". It should come as a surprise to you, goddamm noob, that just two programmers is quorum for never-ending my-code-is-prettier war. With each commit both guys spend more and more time rewriting parts of program considered done. Those who miss the point yet attempt to mediate usually say "Why don't you leave the ready code as is? Why don't you work on absent features instead of spoiling finished ones?". What bothers me most is how anyone could sound so insulting and get away with it. Afterall, one must realize the equivalence is to state the person has bad judgement, is unable to work without supervision and thus is unfit for the project.

If you smirked a little, then let me state it explicitly. Yes, I write from my experience. And no, I disagree. The singular reason why any programmer rewrites any established, ready and tested functionality is that "the bigger picture" failed to capture their mind.

You could force your programmers to add new code atop existing. However, you will inevitably witness slower and slower pace of development. What you failed to understand is when there is no big picture, no idea behind how and what the program does the simple dude has to remember more and more unrelated tricks and hacks that grease the wheels. By "playing safe" you just made that simple program as over the top complicated as human anatomy. The thing about anatomy is that it is finished. A completly new design is not going to arrive next year. With software - yes, it will - next month.

With state machine as the main design principle I could believe the work pace in my team is going to be steady and reasonable. Now we can attempt to make one. And yes, I know your thoughts. Why bother? Boost did that already. Two reasons. First, have you tried it? I have. The horrible experiance still haunts me. Second, Boost meta state machine is so over-the-top I believe there must be a simpler way.

Let's start with basic requirements. I want my state machine to be:

* finite; with all possible states known at compile time
* all states should be listed in definition
* no other features like, for instance, guards
* transitions can be done manually by programmer.

I got the solution all of you should be happy with. First, no need for third party libraries. Second, is packed with standard library. Third, is designed by C++ experts. The solution is: `std::variant`. What? You don't like it? So what it is C++17? How is it experimental in 2019? We are preparing for C++20 so C++17 is workaday. Is not supported? I have successfully built C++17 applications for arm. No tricks are needed.

What puzzles me most is how ungrateful people are. I give you a solution you need but you still insist it is not the solution you need. Each time it goes like this. First, you express a problem. Second, I solve the problem. Third, you reject my solution. Forth, you choose to use a third-party library you know nothing about even though it does not solve our problem. A few rounds like these and now I see clearly the cause. What you are looking for is not any solution but authority you can trust.

None understands tools available. Use boost::asio but avoid posix because it is too old. Use abstractions but avoid templates because they are too complicated. Use third-party libraries because they are made by deities of superb intellect and C++ expertise. The more I come across these and similar arguments the more I am convinced people don't learn. Each one arrives at the time in their live when they stop to grow. And from that point they deteriorate. Until, finally, their heart calls it quits so the rotting body can by burried. The only question is how to deal with these zombies when they still walk and spread obsolete gospel?

"You cannot learn an old dog new tricks". In this proverb we have the primal reason why you reject `std::variant`. Nothing is perfect. Everybody knows that. But to say in one sentence both "you should use libraries made by experts" and "you should avoid templates" is absurd. C++ committee designs entire language around template features. Both stl and boost are packed with these. How you can advise boost and mock commitee at the same time. "We are not committee". To avoid templates means to make wrong use of C++. Clearly. You lack qualifications to call yourself a C++ programmer.

But the abyss goes deeper. Google code style advises to avoid metaprogramming. Most programmers don't understand metaprogramming so they think it means to avoid templates all together. Another advice is to avoid non-const references since they are "can be confusing, as they have value syntax but pointer semantics". As I see it the good old language which is C was hijacked by a band of hippies and all of us was taken for a ride. Without a doubt generic programming is great and without a doubt templates suck. There can be no resolution. The divide between new C++ and accepted style will grow. I am certain that to start a new language with templates done right was much harder then to hijack C.

Anyway, we have to somehow cross the abyss. To sum up

* `std::variant` is a new toy children play today but you are too old to play with them
* `std::variant` is a template and as any other template produces horrific logs
* you have now no skill since you were too lazy to learn templates in your youth
* you are too old to learn so you frantically look for authority to ease you fear
* When I ignore your incompetence, they tell me I lack "soft skills"

As you can see it is hard to get anywhere with obstacles like these. And we barely started. Oh, boy. Since I cannot teach you basics I will do what I cannot in real life. I say "you don't like it? get lost!" then proceed with our state machine.

The first attempt, even though crude, gives me hope. The best thing about generic programming is it helps you prove your program works. All possible states are known and checked at compile time. Every path your program takes is verified even before first run. At least it should be. Most programmers are attracted to dynamic structures. Use `std::vector` not `std::aray`. Use `std::shared_ptr` avoid regular pointers. What they want is for RAII to guarantee program is valid, no one wants to prove the program works. Who am I kidding? I work with technicians. When I say "prove" they look at me like a dog at a man and think "when will he throw the ball?". Our state machine is proven to work since you cannot do anything outside defined operations. And that is our goal.

Next step should be getting our code out of repetetive structured programming (the big if-else). Let's apply visitor pattern. With `std::visit` it should be easy. As a byproduct we moved to object oriented paradigm. Whether it is a benefit it yet remains to be seen. However, what is of great importance is whether state machine can have many transitions from the same state.
